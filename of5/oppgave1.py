#oppgave 1
#a) Lag en liste med tallene fra 0 til 9 og skriv ut listen

tallliste = [0,1,2,3,4,5,6,7,8,9]
tallliste1 = [i for i in range(10)]

#b) Endre det siste tallet i listen til 5 og skriv ut listen igjen

tallliste1[-1] = 5

#c) Lag en funksjon som returnerer første halvdelen av en liste, og bruk den på listen vår 
# Hint: slicing: liste[a:b]

def halv_liste(liste):
    return liste[:len(liste)//2]

#d) Lag en funksjon som kopierer alt utenom det første og siste elementet i en ny liste, og returnerer den nye listen. 
#Hint: len() fungerer også på lister!

def bortsett_fra_første_og_siste(liste):
    return liste[1:-1]

print(bortsett_fra_første_og_siste(tallliste))

